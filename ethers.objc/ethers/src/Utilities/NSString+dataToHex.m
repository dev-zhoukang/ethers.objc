//
//  NSString+dataToHex.m
//  UGWallet-iOS
//
//  Created by hoolai on 9/4/17.
//  Copyright © 2017 shinolr_go. All rights reserved.
//

#import "NSString+dataToHex.h"

@implementation NSString (dataToHex)

+ (NSString *)convertDataToHexStrWithData:(NSData *)data {
    if (!data || [data length] == 0) {
        return @"";
    }
    NSMutableString *string = [[NSMutableString alloc] initWithCapacity:[data length]];
    
    [data enumerateByteRangesUsingBlock:^(const void *bytes, NSRange byteRange, BOOL *stop) {
        unsigned char *dataBytes = (unsigned char*)bytes;
        for (NSInteger i = 0; i < byteRange.length; i++) {
            NSString *hexStr = [NSString stringWithFormat:@"%x", (dataBytes[i]) & 0xff];
            if ([hexStr length] == 2) {
                [string appendString:hexStr];
            } else {
                [string appendFormat:@"0%@", hexStr];
            }
        }
    }];
    return string;
}

@end
