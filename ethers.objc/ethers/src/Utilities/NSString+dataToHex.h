//
//  NSString+dataToHex.h
//  UGWallet-iOS
//
//  Created by hoolai on 9/4/17.
//  Copyright © 2017 shinolr_go. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (dataToHex)

/**
 NSData 转 16 进制字符串

 @param data 私钥的 NSData 格式
 @return 16 进制字符串
 */
+ (NSString *)convertDataToHexStrWithData:(NSData *)data;

@end
